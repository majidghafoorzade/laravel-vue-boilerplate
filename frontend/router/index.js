import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    //base: '/admin/',
    routes: [{
            path: '/',
            name: 'Dashboard',
            component: () =>
                import ( /* webpackChunkName: "dashboard" */ '../pages/Dashboard'),
        },
        {
            path: '/auth/login',
            name: 'Login',
            component: () =>
                import ( /* webpackChunkName: "login" */ '../pages/Login'),
        },
    ],
});


export default router;