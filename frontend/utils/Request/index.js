export default {
    methods: {
        async Request(type, data = null) {
            const { action } = await
            import ('./Types/' + type);
            action(data);
        }
    }
}