const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('frontend/app.js', 'public/js');

mix.extend('rewriteRules', webpackConfig => {
    let scssRule = webpackConfig.module.rules.find(rule => String(rule.test) === '/\\.scss$/');

    scssRule.loaders[1] = { /* css-loader */
        loader: 'css-loader',
        options: {
            modules: true,
            importLoaders: 1,
            localIdentName: '[local]_[hash:base64:4]'
        }
    };
});

mix.webpackConfig({
    output: {
        chunkFilename: 'chunks/[name].js', //replace with your path
    },
});

mix.rewriteRules();